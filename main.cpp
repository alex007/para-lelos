#include <cstdio>
#include <map>
#include <iostream>
#include <pthread.h>
using namespace std;

#define SIZE 100
#define NTHREADS 4
#define NMATRIX 4

double ***M, **A, **B, **C, **D;
double PRODUCT[2][SIZE][SIZE], SUMA[SIZE][SIZE];
int a(int i, int j){return i + j;}
int b(int i, int j){return i + 2 * j;}
int c(int i, int j){return i * 2 + j * 3;}
int d(int i, int j){return 2 * i + j;}

int (*finit[NMATRIX])(int, int);
pthread_t hilos[NTHREADS];
typedef struct{int begin, end;} thr_param;

thr_param param[NTHREADS];


void init_space(){
    M = new double**[NMATRIX];
    for(int i=0; i< NMATRIX; ++i){
        M[i] = new double*[SIZE];
        for(int j = 0; j< SIZE; ++j)
            M[i][j] = new double[SIZE];
    }
}

void* init(void *ptr){
    thr_param *hilo = (thr_param *) ptr;
    for(int i=0; i< NMATRIX; ++i)
        for(int j=hilo->begin; j< hilo->end; ++j)
            for(int k=0; k< SIZE; ++k)
                M[i][j][k]=finit[i](j, k);
}
void* multi(void *ptr){
    thr_param *hilo = (thr_param *) ptr;
    for(int i=hilo->begin; i< hilo->end; ++i)
        for(int j=0; j < SIZE; ++j){
            for(int k=0; k < SIZE; ++k){
                PRODUCT[0][i][j]+= A[i][k] + B[k][j];
                PRODUCT[1][i][j]+= C[i][k] + D[k][j];
            }
        }
}

void* suma(void *ptr){
    thr_param *hilo = (thr_param *) ptr;
    for(int i=hilo->begin; i< hilo->end; ++i)
        for(int j=0; j < SIZE; ++j){
            SUMA[i][j]+= PRODUCT[0][i][j] + PRODUCT[1][i][j];
        }
}
int main()
{
    init_space();
    void *r;
    int partition = SIZE/NTHREADS;
    finit[0]=a;finit[1]=b;finit[2]=c;finit[3]=d;
    for(int i=0; i< NTHREADS; ++i){
        param[i].begin = i * partition;
        param[i].end = (i + 1) * partition;
    }

    param[NTHREADS-1].end=SIZE;

    for(int i=0; i< NTHREADS; ++i)
        pthread_create(&hilos[i], NULL, init, &param[i]);

    for(int i=0; i< NTHREADS; ++i)
        pthread_join(hilos[i], &r);
    cout<<"end initialization!"<<endl;

    A = M[0]; B = M[1]; C = M[2]; D = M[3];
    for(int i=0; i< NTHREADS; ++i)
        pthread_create(&hilos[i], NULL, multi, &param[i]);
    for(int i=0; i< NTHREADS; ++i)
        pthread_join(hilos[i], &r);

    for(int i=0; i< NTHREADS; ++i)
        pthread_create(&hilos[i], NULL, suma, &param[i]);
    for(int i=0; i< NTHREADS; ++i)
        pthread_join(hilos[i], &r);
    cout<<"end operations!"<<endl;

    return 0;
}
