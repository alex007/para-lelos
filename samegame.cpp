#include <iostream>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <map>
using namespace std;

typedef pair< int, pair<int, int> > bloque;

#define nbloques first
#define R second.first
#define C second.second
#define STEPS 4

int rs[]= {-1, 1, 0, 0}, cs[]= {0, 0, 1, -1};
int rows, cols, pivot, value;

string solution;

void p(int **m)
{
    for(int i=rows-1; i>=0; --i)
    {
        for(int j=0; j< cols; ++j)
            cout<<m[i][j]<<"\t";
        cout<<endl;
    }
}
void agregar_resultado(int r, int c, int **m)
{
    string temp;
    char buffer[10];
    sprintf(buffer,"%d %d\n", r, c);
    temp=buffer;
    for(int i=rows-1; i>=0; --i)
    {
        for(int j=0; j< cols; ++j)
        {
            sprintf(buffer,"%d ", m[i][j]);
            temp+=buffer;
        }
        temp+="\n";
    }
    temp+="\n";
    solution = temp + solution;
}
void init(int ***m)
{
    (*m)= new int*[rows];
    for(int i=0; i< cols; ++i)
        (*m)[i]= new int[cols];
}
void copy_matrix(int **m1, int **m2)
{
    for(int i=0; i< rows; ++i)
        for(int j=0; j< cols; ++j)
            m2[i][j]=m1[i][j];
}

void same(int **m1, int ***m2)
{
    init(m2);
    for(int i=0; i< rows; ++i)
        for(int j=0; j< cols; ++j)
            (*m2)[i][j]=m1[i][j];
}
inline bool inbox(int i, int j)
{
    return i>=0 && j>=0 && j<cols && i<rows;
}
int dfs(int r, int c, int **m )
{
    int vecinos_iguales = 1;
    for(int i=0; i<STEPS ; ++i)
    {
        if( inbox(r + rs[i], c + cs[i]) && m[ r + rs[i] ][ c + cs[i] ] == pivot)
        {
            m[ r + rs[i] ][ c + cs[i] ] = value;
            vecinos_iguales += dfs(r+ rs[i], c+ cs[i], m);
        }
    }
    return vecinos_iguales;
}
void caer(int **m)
{
    //cout<<endl<<"antes de caer"<<endl;
    //p(m);
    bool encontro_cero, encontro_numero, desplazamiento;
    for(int j=0; j< cols; ++j)
    {
        encontro_cero=encontro_numero=desplazamiento=0;
        for(int i=0; i< rows; ++i)
        {
            if (m[i][j]==0 && !encontro_cero)encontro_cero=true;
            if (encontro_cero && !encontro_numero)
            {
                desplazamiento++;
                if (m[i][j])encontro_numero=true;
            }
            if (encontro_numero)
                swap(m[i-desplazamiento][j],m[i][j]);
        }
    }
    //cout<<endl<<"despues de caer"<<endl;
    //p(m);
}
bool columna_vacia(int **m, int c)
{
    for(int i=0; i< rows; ++i)
        if (m[i][c]) return false;
    return true;
}
void barrer_izquierda(int **m)
{
    //cout<<endl<<"antes de barrer"<<endl;
    //p(m);
    bool encontro_cero, encontro_numero, desplazamiento;
    encontro_cero=encontro_numero=desplazamiento=0;
    for(int j=0; j< cols; ++j)
    {
        if (columna_vacia(m, j) && !encontro_cero)encontro_cero=true;
        if (encontro_cero && !encontro_numero)
        {
            desplazamiento++;
            if (!columna_vacia(m, j))encontro_numero=true;
        }
        if (encontro_numero)
            for(int i=0; i< rows; ++i)
                swap(m[i][j-desplazamiento],m[i][j]);
    }
    //cout<<endl<<"despues de barrer"<<endl;
    //p(m);
}
vector<bloque> obtener_jugadas(int **m)
{
    vector<bloque> posibles;
    int facecounter=0, caras;
    for(int i=0; i< rows; ++i)
    {
        for(int j=0; j< cols; ++j)
        {
            if (m[i][j] > 0)
            {
                pivot = m[i][j];
                m[i][j] = value = --facecounter;
                caras = dfs(i, j, m);
                if (caras>1)
                {
                    bloque conjunto;
                    conjunto.R=i;
                    conjunto.C=j;
                    conjunto.nbloques = caras;
                    posibles.push_back(conjunto);
                }
            }
        }
    }
    /*cout<<"caras:"<<endl;
    p(m);
    cout<<endl<<endl;*/
    return posibles;
}
bool puro_cero(int **m)
{
    for(int i=0; i< rows; ++i)
        for(int j=0; j< cols; ++j)
            if (m[i][j]) return false;
    return true;
}
bool solve_same(int **m)
{
    bool game_result;
    int **_m, r, c;
    same(m, &_m);
    vector<bloque>  jugadas = obtener_jugadas(_m);
    p(_m);
    if (!jugadas.size())
        return puro_cero(m);
    sort(jugadas.begin(), jugadas.end());
    for(int i=jugadas.size()-1; i>=0; --i)
    {
        r=jugadas[i].R;
        c=jugadas[i].C;
        copy_matrix(m, _m);
        pivot=_m[r][c];
        _m[r][c]=value=0;
        dfs(r, c, _m);
        //cout<<"jugada en:"<<jugadas[i].R<<", "<<jugadas[i].C<<endl;
        caer(_m);
        barrer_izquierda(_m);
        if (solve_same(_m))
        {
            agregar_resultado(r, c, _m);
            return true;
        }
        //cout<<jugadas[i].nbloques<<" at "<<jugadas[i].R<<"-"<<jugadas[i].C<<endl;
    }
    return false;
}

int main()
{
    freopen("in", "r", stdin);
    cin>>rows>>cols;
    int **m;
    init(&m);
    for(int i=rows-1; i>=0; --i)
        for(int j=0; j< cols; ++j)
            cin>>m[i][j];
    if (solve_same(m)) cout<<solution<<endl;
    else cout<<"el juego no tiene solucion :("<<endl;
    return 0;
}
